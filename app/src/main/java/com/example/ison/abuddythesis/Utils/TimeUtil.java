package com.example.ison.abuddythesis.Utils;

import android.util.Log;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimeUtil {

    Timer timer = new Timer();

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    int counter;

    public void startTimer(){
        timer.scheduleAtFixedRate(new TimerTask(){
            public void run() {
                counter++;
                Log.e("aaaa", "run: " + counter);
                setCounter(counter);
            }
        }, new Date(), 1000);
    }

    public void cancleTimer(){
        timer.cancel();
    }

}
