package com.example.ison.abuddythesis.activities.Emotions;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.activities.Puzzle.LevelNine.LevelNinePuzzleActivity;
import com.example.ison.abuddythesis.activities.Puzzle.LevelSeven.LevelSevenPuzzleActivity;
import com.example.ison.abuddythesis.activities.Puzzle.LevelSix.LevelSixPuzzleActivity;
import com.example.ison.abuddythesis.constants.Constants;
import com.example.ison.abuddythesis.pojo.AppGlobalVariable;

public class LevelSelectEmotionsActivity extends AppCompatActivity implements View.OnClickListener{

    Intent myIntent;
    private Button e_btn_lvl1,e_btn_lvl2, e_btn_lvl3, e_btn_lvl4, e_btn_lvl5,
            e_btn_lvl6, e_btn_lvl7, e_btn_lvl8, e_btn_lvl9, e_btn_lvl10;
    SharedPreferences prefs = null;
    MediaPlayer mediaPlayer, btn_click;
    boolean isMute = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_level_select_emotions);
        prefs = getSharedPreferences("myPreferences", MODE_PRIVATE);
        prefs.getString(Constants.IS_LEVEL1_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL2_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL3_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL4_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL5_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL6_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL7_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL8_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL9_EMOTION_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL10_EMOTION_PASSED, Constants.FALSE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }
        btn_click = MediaPlayer.create(this,R.raw.click);
        initUI();
    }

    private void initUI(){
        e_btn_lvl1 = (Button) findViewById(R.id.e_btn_lvl1);
        e_btn_lvl2 = (Button) findViewById(R.id.e_btn_lvl2);
        e_btn_lvl3 = (Button) findViewById(R.id.e_btn_lvl3);
        e_btn_lvl4 = (Button) findViewById(R.id.e_btn_lvl4);
        e_btn_lvl5 = (Button) findViewById(R.id.e_btn_lvl5);
        e_btn_lvl6 = (Button) findViewById(R.id.e_btn_lvl6);
        e_btn_lvl7 = (Button) findViewById(R.id.e_btn_lvl7);
        e_btn_lvl8 = (Button) findViewById(R.id.e_btn_lvl8);
        e_btn_lvl9 = (Button) findViewById(R.id.e_btn_lvl9);
        e_btn_lvl10 = (Button) findViewById(R.id.e_btn_lvl10);
        e_btn_lvl1.setOnClickListener(this);
        e_btn_lvl2.setOnClickListener(this);
        e_btn_lvl3.setOnClickListener(this);
        e_btn_lvl4.setOnClickListener(this);
        e_btn_lvl5.setOnClickListener(this);
        e_btn_lvl6.setOnClickListener(this);
        e_btn_lvl7.setOnClickListener(this);
        e_btn_lvl8.setOnClickListener(this);
        e_btn_lvl9.setOnClickListener(this);
        e_btn_lvl10.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.e_btn_lvl1:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelOneEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl2:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelTwoEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl3:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelThreeEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl4:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelFourEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl5:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelFiveEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl6:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelSixEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl7:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelSevenEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl8:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelEightEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl9:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelNineEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            case R.id.e_btn_lvl10:

                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelTenEmotionsActivity.class);
                this.startActivity(myIntent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
        if (isMute == true) {
            mediaPlayer.stop();
        }
        else{
            //Do Nothing
        }

        if(prefs.getString(Constants.IS_LEVEL1_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl2.setEnabled(false);
            e_btn_lvl2.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl2.setEnabled(true);
            e_btn_lvl2.setBackgroundResource(R.drawable.p_btn_lvl2);
        }

        if(prefs.getString(Constants.IS_LEVEL2_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl3.setEnabled(false);
            e_btn_lvl3.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl3.setEnabled(true);
            e_btn_lvl3.setBackgroundResource(R.drawable.p_btn_lvl3);
        }

        if(prefs.getString(Constants.IS_LEVEL3_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl4.setEnabled(false);
            e_btn_lvl4.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl4.setEnabled(true);
            e_btn_lvl4.setBackgroundResource(R.drawable.p_btn_lvl4);
        }

        if(prefs.getString(Constants.IS_LEVEL4_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl5.setEnabled(false);
            e_btn_lvl5.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl5.setEnabled(true);
            e_btn_lvl5.setBackgroundResource(R.drawable.p_btn_lvl5);
        }

        if(prefs.getString(Constants.IS_LEVEL5_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl6.setEnabled(false);
            e_btn_lvl6.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl6.setEnabled(true);
            e_btn_lvl6.setBackgroundResource(R.drawable.p_btn_lvl6);
        }

        if(prefs.getString(Constants.IS_LEVEL6_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl7.setEnabled(false);
            e_btn_lvl7.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl7.setEnabled(true);
            e_btn_lvl7.setBackgroundResource(R.drawable.p_btn_lvl7);
        }

        if(prefs.getString(Constants.IS_LEVEL7_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl8.setEnabled(false);
            e_btn_lvl8.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl8.setEnabled(true);
            e_btn_lvl8.setBackgroundResource(R.drawable.p_btn_lvl8);
        }

        if(prefs.getString(Constants.IS_LEVEL8_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl9.setEnabled(false);
            e_btn_lvl9.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl9.setEnabled(true);
            e_btn_lvl9.setBackgroundResource(R.drawable.p_btn_lvl9);
        }

        if(prefs.getString(Constants.IS_LEVEL9_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            e_btn_lvl10.setEnabled(false);
            e_btn_lvl10.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            e_btn_lvl10.setEnabled(true);
            e_btn_lvl10.setBackgroundResource(R.drawable.p_btn_lvl10);
        }

        if(prefs.getString(Constants.IS_LEVEL10_EMOTION_PASSED, Constants.FALSE).contains(Constants.FALSE)){

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }
}
