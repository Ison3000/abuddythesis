package com.example.ison.abuddythesis.activities.Emotions;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.Utils.TimeUtil;
import com.example.ison.abuddythesis.constants.Constants;
import com.example.ison.abuddythesis.dialog.ViewDialog;

import java.util.ArrayList;
import java.util.Collections;

public class LevelThreeEmotionsActivity extends AppCompatActivity {
    LinearLayout ll, top_compte;
    int id = 1;
    ViewDialog alert;
    SharedPreferences prefs = null;
    MediaPlayer mediaPlayer, btn_click;
    boolean isMute = false;
    TimeUtil timeUtil = new TimeUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_level_three_emotions);
        ll =  (LinearLayout)findViewById(R.id.llShuffleBox);
        timeUtil.startTimer();
        prefs = getSharedPreferences("myPreferences", MODE_PRIVATE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }
        btn_click = MediaPlayer.create(this,R.raw.click);
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer.setLooping(true);
        top_compte =  (LinearLayout)findViewById(R.id.top_compte);

        ArrayList<Button> buttonList = new ArrayList<Button>();

        for (int i = 0; i < 3; i++) {
            final Button b = new Button(this);
            b.setGravity(Gravity.CENTER_HORIZONTAL);
            b.setId(generateUniqueId());                    // Set an id to Button
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dealWithButtonClick(b);
                }
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(50, 10, 50, 10);
            b.setLayoutParams(params);

            switch(b.getId()){
                case 1:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_angry));
                    break;
                case 2:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_happy));
                    break;
                case 3:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_sad));
                    break;
                default:
                    break;
            }
            buttonList.add(b);
        }

// Shuffle
        Collections.shuffle(buttonList);
        for (int i = 0; i < 3; i++) {
            if (i < 3) {
                top_compte.addView(buttonList.get(i));
            } else {
                //Do Nothing
            }
        }
    }

    public void dealWithButtonClick(Button b) {
        switch(b.getId()) {
            case 1:
                if(!isMute){
                    btn_click.start();
                }
//                Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
                alert = new ViewDialog();
                timeUtil.cancleTimer();
                alert.showDialog(this, Constants.CORRECT, true,  timeUtil.getCounter());
                prefs.edit().putString(Constants.IS_LEVEL3_EMOTION_PASSED, Constants.TRUE).commit();
                int medal;
                medal = prefs.getInt(Constants.MEDAL_COUNT, 0) + 1;
                prefs.edit().putInt(Constants.MEDAL_COUNT, medal).commit();
                break;

            case 2:
                if(!isMute){
                    btn_click.start();
                }
//                Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();

                break;

            case 3:
                if(!isMute){
                    btn_click.start();
                }
//                Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }


    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isMute) {
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }
}
