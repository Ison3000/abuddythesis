package com.example.ison.abuddythesis.activities.Emotions;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.ison.abuddythesis.R;

import java.util.ArrayList;
import java.util.Collections;

public class SampleEmotionsActivity extends AppCompatActivity {

    LinearLayout ll, top_compte;
    int id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_sample_emotions);

        ll =  (LinearLayout)findViewById(R.id.llShuffleBox);

        top_compte =  (LinearLayout)findViewById(R.id.top_compte);

        ArrayList<Button> buttonList = new ArrayList<Button>();

        for (int i = 0; i < 3; i++) {
            Button b = new Button(this);
            b.setGravity(Gravity.CENTER_HORIZONTAL);
            b.setId(generateUniqueId());                    // Set an id to Button
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(50, 10, 50, 10);
            b.setLayoutParams(params);

            switch(b.getId()){
                case 1:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_angry));
                    break;
                case 2:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_happy));
                    break;
                case 3:
                    b.setBackground(getResources().getDrawable(R.drawable.emo_sad));
                    break;
                default:
                    break;
            }
            buttonList.add(b);
        }

// Shuffle
        Collections.shuffle(buttonList);
        for (int i = 0; i < 3; i++) {
            if (i < 3) {
                top_compte.addView(buttonList.get(i));
            } else {
            //Do Nothing
            }
        }
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }
}
