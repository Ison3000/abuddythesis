package com.example.ison.abuddythesis.activities;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.ison.abuddythesis.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HelpActivity extends AppCompatActivity {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final Integer[] GUIDE= {R.drawable.guide_1,R.drawable.guide_2,R.drawable.guide_3,R.drawable.guide_4,
            R.drawable.guide_5,R.drawable.guide_6,R.drawable.guide_7};
    private ArrayList<Integer> GUIDEArray = new ArrayList<Integer>();
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_help);
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
        init();
    }
    private void init() {
        for(int i=0;i<GUIDE.length;i++)
            GUIDEArray.add(GUIDE[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyAdapter(HelpActivity.this,GUIDEArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == GUIDE.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 0, 10000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }

}
