package com.example.ison.abuddythesis.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.Utils.SharedPref;
import com.example.ison.abuddythesis.pojo.AppGlobalVariable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_start, btn_exit;
    MediaPlayer mediaPlayer, mediaPlayer1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);
        initUI();
    }



    private void initUI(){
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer1 = MediaPlayer.create(this,R.raw.bg_music);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_start.setOnClickListener(this);
        btn_exit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_exit:
                finish();
                break;

            case R.id.btn_start:
                goToActivity();
                break;

             default:
                 break;

        }
    }


    private void goToActivity(){
        Intent myIntent = new Intent(this, SelectionActivity.class);
//        myIntent.putExtra("key", value); //Optional parameters
        this.startActivity(myIntent);
        }


}
