package com.example.ison.abuddythesis.activities.Puzzle.PuzzleSample;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.ison.abuddythesis.R;

public class PuzzleTutActivity extends AppCompatActivity {

    private ViewGroup main;
    private ImageView image, image2;

    private int xDelta, yDelta, array2[] = new int[2], array[] = new int[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_tut);
        main = (RelativeLayout) findViewById(R.id.main);
        image = (ImageView) findViewById(R.id.image);
        image2 = (ImageView) findViewById(R.id.image2);

        image.setOnTouchListener(onTouchListener());

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
   getDetails();
    }

    private void getDetails(){
        image.getLocationOnScreen(array);
        image2.getLocationOnScreen(array2);
        Log.e("aaa", "onWindowFocusChanged: " + array[0] + " " + array[1] + " "+ array2[0] + " " + array2[1] );

        Log.e("aaa", "onWindowFocusChanged: " + (array[0] - array2[0]));
        Log.e("aaa", "onWindowFocusChanged: " + (array[1] - array2[1]));
        if(isCorrect(array[0] - array2[0], array[1] - array2[1])){

        }
    }

    private boolean isCorrect(int x, int y){

        if(x < 5 && y < 5){
            if(x > -5 && y> -5){
                Log.e("aaa", "true" );
                return true;
            }
            else
                Log.e("aaa", "false" );
            return false;
        }
        else
            Log.e("aaa", "false" );
        return false;
    }


    private View.OnTouchListener onTouchListener() {
        return new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)
                                view.getLayoutParams();

                        xDelta = x - lParams.leftMargin;
                        yDelta = y - lParams.topMargin;
                        break;

                    case MotionEvent.ACTION_UP:
                        getDetails();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                .getLayoutParams();
                        layoutParams.leftMargin = x - xDelta;
                        layoutParams.topMargin = y - yDelta;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        view.setLayoutParams(layoutParams);
                        break;
                }
                main.invalidate();
                return true;
            }
        };
    }

}
