package com.example.ison.abuddythesis.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.Utils.SharedPref;
import com.example.ison.abuddythesis.activities.Emotions.LevelSelectEmotionsActivity;
import com.example.ison.abuddythesis.activities.Puzzle.LevelSelectActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelOne.LevelOneShapesActvity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelSelectShapesActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.Tests.SampleShapesActivity;
import com.example.ison.abuddythesis.constants.Constants;
import com.example.ison.abuddythesis.pojo.AppGlobalVariable;


public class SelectionActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_sound, btn_puzzle, btn_expresion, btn_shapes, btn_back, btn_help;
    Intent myIntent;
    MediaPlayer mediaPlayer,btn_click;
    ImageView medal;
    TextView medalCount;

    SharedPreferences prefs = null;
    boolean isMute = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_selection);
        prefs = getSharedPreferences("myPreferences", MODE_PRIVATE);
        btn_click = MediaPlayer.create(this,R.raw.click);
        initUI();
    }

    public void initUI(){
        medalCount = (TextView) findViewById(R.id.medal_count);
        medal = (ImageView) findViewById(R.id.medal);
        btn_sound = (Button) findViewById(R.id.btn_sound);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_expresion = (Button) findViewById(R.id.btn_expression);
        btn_puzzle = (Button) findViewById(R.id.btn_puzzle);
        btn_shapes = (Button) findViewById(R.id.btn_shapes);
        btn_help = (Button) findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);
        btn_sound.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_shapes.setOnClickListener(this);
        btn_puzzle.setOnClickListener(this);
        btn_expresion.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_back:
                    finish();
                    mediaPlayer.stop();
                    break;

                case R.id.btn_sound:
                    btn_click.start();
                    if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
                        isMute = true;
                        btn_sound.setBackgroundResource( R.drawable.sound);
                        mediaPlayer.start();
                        prefs.edit().putString(Constants.IS_MUTE, Constants.TRUE).commit();
                    }
                    else{
                        btn_sound.setBackgroundResource( R.drawable.mute);
                        isMute = false;
                        mediaPlayer.pause();
                        prefs.edit().putString(Constants.IS_MUTE, Constants.FALSE).commit();
                    }
                    break;

                case R.id.btn_expression:
                    if(isMute){
                        btn_click.start();
                    }

                    myIntent = new Intent(this, LevelSelectEmotionsActivity.class);
                    mediaPlayer.stop();
                    this.startActivity(myIntent);
                    break;

                case R.id.btn_help:
                    if(isMute){
                        btn_click.start();
                    }

                    myIntent = new Intent(this, HelpActivity.class);
                    mediaPlayer.stop();
                    this.startActivity(myIntent);
                    break;

                case R.id.btn_shapes:
                    if(isMute){
                        btn_click.start();
                    }
                    myIntent = new Intent(this, LevelSelectShapesActivity.class);
                    mediaPlayer.stop();
                    this.startActivity(myIntent);
                    break;

                case R.id.btn_puzzle:
                    if(isMute){
                        btn_click.start();
                    }
                    myIntent = new Intent(this, LevelSelectActivity.class);
                    mediaPlayer.stop();
                    this.startActivity(myIntent);
                    break;

                default:
                    break;

            }

        }

    @Override
    protected void onResume() {
        super.onResume();
        medalCount.setText("x"+String.valueOf(prefs.getInt(Constants.MEDAL_COUNT, 0)));

        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        if (prefs.getBoolean("firstrun", true)) {
            prefs.edit().putBoolean("firstrun", false).commit();
            prefs.edit().putString(Constants.IS_MUTE, Constants.TRUE).commit();
            myIntent = new Intent(this, HelpActivity.class);
            mediaPlayer.stop();
            this.startActivity(myIntent);
        }

        if (prefs.getString(Constants.IS_MUTE, Constants.TRUE).contains(Constants.TRUE)) {
            btn_sound.setBackgroundResource( R.drawable.sound);
            prefs.edit().putString(Constants.IS_MUTE, Constants.FALSE);
        }
        else{
            btn_sound.setBackgroundResource( R.drawable.mute);
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }
}
