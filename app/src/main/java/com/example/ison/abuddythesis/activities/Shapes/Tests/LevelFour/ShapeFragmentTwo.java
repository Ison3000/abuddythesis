package com.example.ison.abuddythesis.activities.Shapes.Tests.LevelFour;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.constants.Constants;
import com.example.ison.abuddythesis.dialog.ViewDialog;

public class ShapeFragmentTwo extends Fragment implements View.OnClickListener {

    protected DrawingViewTwo mDrawingView;
    Button btn_line, btn_smooth_line, btn_rectangle;
    SharedPreferences prefs = null;
    MediaPlayer mediaPlayer, btn_click;
    boolean isMute = false;
    public ShapeFragmentTwo(){
        super();
    }

    ViewDialog alert;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_level4, container, false);
        mDrawingView = (DrawingViewTwo) rootView.findViewById(R.id.drawingview);

        btn_line = (Button) rootView.findViewById(R.id.btn_line);
        btn_smooth_line = (Button) rootView.findViewById(R.id.btn_smooth_line);
        btn_rectangle = (Button) rootView.findViewById(R.id.btn_rectangle);
        btn_line.setOnClickListener(this);
        btn_smooth_line.setOnClickListener(this);
        btn_rectangle.setOnClickListener(this);
        mDrawingView.mCurrentShape = DrawingViewTwo.SMOOTHLINE;
        mDrawingView.mColor = Constants.GREEN;
        mDrawingView.reset();
        prefs = getActivity().getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }
        btn_click = MediaPlayer.create(getActivity(),R.raw.click);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_line:
                if(!isMute){
                    btn_click.start();
                }
                mDrawingView.mColor = Constants.BLUE;
                mDrawingView.reset();
                break;
            case R.id.btn_smooth_line:
                if(!isMute){
                    btn_click.start();
                }
                mDrawingView.mColor = Constants.RED;
                mDrawingView.reset();
                break;
            case R.id.btn_rectangle:
                if(!isMute){
                    btn_click.start();
                }
                mDrawingView.mColor = Constants.GREEN;
                mDrawingView.reset();
                break;

            default:
                break;
        }
    }

    public void showDialog(){
        alert = new ViewDialog();
        alert.showDialog(getActivity(), Constants.SUCCESS, true);
    }

}
