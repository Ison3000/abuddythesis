package com.example.ison.abuddythesis.activities.Shapes.Tests;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelEight.LevelEightShapeActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelFive.LevelFiveShapeActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelFour.LevelFourShapeActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelNine.LevelNineShapesActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelOne.LevelOneShapesActvity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelSeven.LevelSevenShapeActvity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelSix.LevelSixShapeActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelTen.LevelTenShapeAvtivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelThree.LevelThreeShapesActivity;
import com.example.ison.abuddythesis.activities.Shapes.Tests.LevelTwo.LevelTwoShapesActvity;
import com.example.ison.abuddythesis.constants.Constants;

public class LevelSelectShapesActivity extends AppCompatActivity implements View.OnClickListener{

    private Button p_btn_lvl1,p_btn_lvl2, p_btn_lvl3, p_btn_lvl4, p_btn_lvl5,
            p_btn_lvl6, p_btn_lvl7, p_btn_lvl8, p_btn_lvl9, p_btn_lvl10;
    Intent myIntent;
    SharedPreferences prefs = null;
    MediaPlayer mediaPlayer, btn_click;
    boolean isMute = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_level_select_shapes);
        prefs = getSharedPreferences("myPreferences", MODE_PRIVATE);
        prefs.getString(Constants.IS_LEVEL1_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL2_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL3_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL4_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL5_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL6_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL7_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL8_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL9_SHAPE_PASSED, Constants.FALSE);
        prefs.getString(Constants.IS_LEVEL10_SHAPE_PASSED, Constants.FALSE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }
        btn_click = MediaPlayer.create(this,R.raw.click);
        initUI();
    }

    private void initUI(){
        p_btn_lvl1 = (Button) findViewById(R.id.p_btn_lvl1);
        p_btn_lvl2 = (Button) findViewById(R.id.p_btn_lvl2);
        p_btn_lvl3 = (Button) findViewById(R.id.p_btn_lvl3);
        p_btn_lvl4 = (Button) findViewById(R.id.p_btn_lvl4);
        p_btn_lvl5 = (Button) findViewById(R.id.p_btn_lvl5);
        p_btn_lvl6 = (Button) findViewById(R.id.p_btn_lvl6);
        p_btn_lvl7 = (Button) findViewById(R.id.p_btn_lvl7);
        p_btn_lvl8 = (Button) findViewById(R.id.p_btn_lvl8);
        p_btn_lvl9 = (Button) findViewById(R.id.p_btn_lvl9);
        p_btn_lvl10 = (Button) findViewById(R.id.p_btn_lvl10);
        p_btn_lvl1.setOnClickListener(this);
        p_btn_lvl2.setOnClickListener(this);
        p_btn_lvl3.setOnClickListener(this);
        p_btn_lvl4.setOnClickListener(this);
        p_btn_lvl5.setOnClickListener(this);
        p_btn_lvl6.setOnClickListener(this);
        p_btn_lvl7.setOnClickListener(this);
        p_btn_lvl8.setOnClickListener(this);
        p_btn_lvl9.setOnClickListener(this);
        p_btn_lvl10.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.p_btn_lvl1:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelOneShapesActvity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl2:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelTwoShapesActvity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl3:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelThreeShapesActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl4:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelFourShapeActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl5:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelFiveShapeActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl6:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelSixShapeActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl7:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelSevenShapeActvity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl8:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelEightShapeActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl9:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelNineShapesActivity.class);
                startActivity(myIntent);
                break;
            case R.id.p_btn_lvl10:
                if(!isMute){
                    btn_click.start();
                }
                myIntent = new Intent(this, LevelTenShapeAvtivity.class);
                startActivity(myIntent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
        if (prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)) {
            mediaPlayer.stop();
        }
        else{
            //Do Nothing
        }

        if(prefs.getString(Constants.IS_LEVEL1_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl2.setEnabled(false);
            p_btn_lvl2.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl2.setEnabled(true);
            p_btn_lvl2.setBackgroundResource(R.drawable.p_btn_lvl2);
        }

        if(prefs.getString(Constants.IS_LEVEL2_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl3.setEnabled(false);
            p_btn_lvl3.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl3.setEnabled(true);
            p_btn_lvl3.setBackgroundResource(R.drawable.p_btn_lvl3);
        }

        if(prefs.getString(Constants.IS_LEVEL3_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl4.setEnabled(false);
            p_btn_lvl4.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl4.setEnabled(true);
            p_btn_lvl4.setBackgroundResource(R.drawable.p_btn_lvl4);
        }

        if(prefs.getString(Constants.IS_LEVEL4_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl5.setEnabled(false);
            p_btn_lvl5.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl5.setEnabled(true);
            p_btn_lvl5.setBackgroundResource(R.drawable.p_btn_lvl5);
        }

        if(prefs.getString(Constants.IS_LEVEL5_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl6.setEnabled(false);
            p_btn_lvl6.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl6.setEnabled(true);
            p_btn_lvl6.setBackgroundResource(R.drawable.p_btn_lvl6);
        }

        if(prefs.getString(Constants.IS_LEVEL6_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl7.setEnabled(false);
            p_btn_lvl7.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl7.setEnabled(true);
            p_btn_lvl7.setBackgroundResource(R.drawable.p_btn_lvl7);
        }

        if(prefs.getString(Constants.IS_LEVEL7_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl8.setEnabled(false);
            p_btn_lvl8.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl8.setEnabled(true);
            p_btn_lvl8.setBackgroundResource(R.drawable.p_btn_lvl8);
        }

        if(prefs.getString(Constants.IS_LEVEL8_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl9.setEnabled(false);
            p_btn_lvl9.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl9.setEnabled(true);
            p_btn_lvl9.setBackgroundResource(R.drawable.p_btn_lvl9);
        }

        if(prefs.getString(Constants.IS_LEVEL9_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){
            p_btn_lvl10.setEnabled(false);
            p_btn_lvl10.setBackgroundResource(R.drawable.btn_lock);
        }
        else{
            p_btn_lvl10.setEnabled(true);
            p_btn_lvl10.setBackgroundResource(R.drawable.p_btn_lvl10);
        }

        if(prefs.getString(Constants.IS_LEVEL10_SHAPE_PASSED, Constants.FALSE).contains(Constants.FALSE)){

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }
}
