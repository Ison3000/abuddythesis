package com.example.ison.abuddythesis.activities.Shapes.Tests.LevelThree;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.constants.Constants;

public class LevelThreeShapesActivity extends AppCompatActivity {
    SharedPreferences prefs = null;
    MediaPlayer mediaPlayer, btn_click;
    boolean isMute = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_level_three_shapes);
        prefs = getSharedPreferences("myPreferences", MODE_PRIVATE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }
        mediaPlayer = MediaPlayer.create(this,R.raw.bg_music);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new ShapeFragmentTwo())
                    .commit();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!isMute) {
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }
}
