/*
 * Copyright 2014 Gabriele Mariotti
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.ison.abuddythesis.activities.Shapes.Tests.Tests;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.constants.Constants;
import com.example.ison.abuddythesis.dialog.ViewDialog;

/**
 * @author Gabriele Mariotti (gabri.mariotti@gmail.com)
 */
public class ShapeFragment extends Fragment implements View.OnClickListener {

    protected DrawingView mDrawingView;
    Button btn_line, btn_smooth_line, btn_rectangle;

    public ShapeFragment(){
        super();
    }

    ViewDialog alert;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mDrawingView = (DrawingView) rootView.findViewById(R.id.drawingview);

        btn_line = (Button) rootView.findViewById(R.id.btn_line);
        btn_smooth_line = (Button) rootView.findViewById(R.id.btn_smooth_line);
        btn_rectangle = (Button) rootView.findViewById(R.id.btn_rectangle);
        btn_line.setOnClickListener(this);
        btn_smooth_line.setOnClickListener(this);
        btn_rectangle.setOnClickListener(this);
        mDrawingView.mCurrentShape = DrawingView.SMOOTHLINE;
        mDrawingView.mColor = Constants.GREEN;
        mDrawingView.reset();
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_line:
                mDrawingView.mColor = Constants.BLUE;
                mDrawingView.reset();
                break;
            case R.id.btn_smooth_line:
                mDrawingView.mColor = Constants.RED;
                mDrawingView.reset();
                break;
            case R.id.btn_rectangle:
                mDrawingView.mColor = Constants.GREEN;
                mDrawingView.reset();
                break;

            default:
                break;
        }
    }

    public void showDialog(){
        alert = new ViewDialog();
        alert.showDialog(getActivity(), Constants.SUCCESS, true);
    }

}
