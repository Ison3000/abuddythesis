package com.example.ison.abuddythesis.constants;

public class Constants {
    public static String SUCCESS = "Success";
    public static String FAILED = "Failed";
    public static String CORRECT = "Correct";
    public static String WRONG = "Wrong";
    public static String RED = "#ff0000";
    public static String GREEN = "#00ff00";
    public static String BLUE = "#0000ff";
    public static String NO_COLOR = "#000000ff";
    public static String TRUE = "TRUE";
    public static String FALSE = "FALSE";
    public static String GREAT = "GREAT";
    public static String GOOD = "GOOD";
    public static String NICE = "NICE";
    public static String VERY_GOOD = "VERY GOOD";
    public static String NULL = "";


    public static String IS_MUTE = "isMute";
    public static String MEDAL_COUNT = "medalCount";
    public static String IS_LEVEL1_SHAPE_PASSED = "isLevel1ShapePassed";
    public static String IS_LEVEL2_SHAPE_PASSED = "isLevel2ShapePassed";
    public static String IS_LEVEL3_SHAPE_PASSED = "isLevel3ShapePassed";
    public static String IS_LEVEL4_SHAPE_PASSED = "isLevel4ShapePassed";
    public static String IS_LEVEL5_SHAPE_PASSED = "isLevel5ShapePassed";
    public static String IS_LEVEL6_SHAPE_PASSED = "isLevel6ShapePassed";
    public static String IS_LEVEL7_SHAPE_PASSED = "isLevel7ShapePassed";
    public static String IS_LEVEL8_SHAPE_PASSED = "isLevel8ShapePassed";
    public static String IS_LEVEL9_SHAPE_PASSED = "isLevel9ShapePassed";
    public static String IS_LEVEL10_SHAPE_PASSED = "isLevel10ShapePassed";

    public static String IS_LEVEL1_EMOTION_PASSED = "isLevel1EmotionPassed";
    public static String IS_LEVEL2_EMOTION_PASSED = "isLevel2EmotionPassed";
    public static String IS_LEVEL3_EMOTION_PASSED = "isLevel3EmotionPassed";
    public static String IS_LEVEL4_EMOTION_PASSED = "isLevel4EmotionPassed";
    public static String IS_LEVEL5_EMOTION_PASSED = "isLevel5EmotionPassed";
    public static String IS_LEVEL6_EMOTION_PASSED = "isLevel6EmotionPassed";
    public static String IS_LEVEL7_EMOTION_PASSED = "isLevel7EmotionPassed";
    public static String IS_LEVEL8_EMOTION_PASSED = "isLevel8EmotionPassed";
    public static String IS_LEVEL9_EMOTION_PASSED = "isLevel9EmotionPassed";
    public static String IS_LEVEL10_EMOTION_PASSED = "isLevel10EmotionPassed";

    public static String IS_LEVEL1_PUZZLE_PASSED = "isLevel1PuzzlePassed";
    public static String IS_LEVEL2_PUZZLE_PASSED = "isLevel2PuzzlePassed";
    public static String IS_LEVEL3_PUZZLE_PASSED = "isLevel3PuzzlePassed";
    public static String IS_LEVEL4_PUZZLE_PASSED = "isLevel4PuzzlePassed";
    public static String IS_LEVEL5_PUZZLE_PASSED = "isLevel5PuzzlePassed";
    public static String IS_LEVEL6_PUZZLE_PASSED = "isLevel6PuzzlePassed";
    public static String IS_LEVEL7_PUZZLE_PASSED = "isLevel7PuzzlePassed";
    public static String IS_LEVEL8_PUZZLE_PASSED = "isLevel8PuzzlePassed";
    public static String IS_LEVEL9_PUZZLE_PASSED = "isLevel9PuzzlePassed";
    public static String IS_LEVEL10_PUZZLE_PASSED = "isLevel10PuzzlePassed";

}
