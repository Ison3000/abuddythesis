package com.example.ison.abuddythesis.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ison.abuddythesis.R;
import com.example.ison.abuddythesis.constants.Constants;

public class ViewDialog {
    TextView text;
    TextView textMsg;
    Button dialogButton;
    ImageView imgView;
    MediaPlayer mediaPlayer;
    SharedPreferences prefs = null;
    boolean isMute = false;

    public void showDialog(final Activity activity, String msg, final Boolean result){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        imgView = (ImageView) dialog.findViewById(R.id.a) ;
        prefs = activity.getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }

        if(result){
            if(!isMute){
                imgView.setBackgroundResource(R.drawable.correct);
                mediaPlayer = MediaPlayer.create(activity,R.raw.success);
                mediaPlayer.start();
            }
        }
        else {
            mediaPlayer = MediaPlayer.create(activity,R.raw.failed);
            mediaPlayer.start();
            imgView.setBackgroundResource(R.drawable.wrong);
        }

        text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(result){
                    activity.finish();
                }
                else{
                    //Do Nothing
                }
            }
        });

        dialog.show();

    }

    public void showDialog(final Activity activity, String msg, final Boolean result, int counter){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        imgView = (ImageView) dialog.findViewById(R.id.a) ;
        Log.e("aaaa", "showDialog: " + counter );
        prefs = activity.getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
        if(prefs.getString(Constants.IS_MUTE, Constants.FALSE).contains(Constants.FALSE)){
            isMute = true;
        }

        if(result){
            if(!isMute){
                imgView.setBackgroundResource(R.drawable.correct);
                mediaPlayer = MediaPlayer.create(activity,R.raw.success);
                mediaPlayer.start();
            }
        }
        else {
            mediaPlayer = MediaPlayer.create(activity,R.raw.failed);
            mediaPlayer.start();
            imgView.setBackgroundResource(R.drawable.wrong);
        }

        text = (TextView) dialog.findViewById(R.id.text_dialog);
        textMsg = (TextView) dialog.findViewById(R.id.text_dialog_msg);

        if(counter > 0 && counter <= 59){
            textMsg.setText(secondsToString(counter)+ "\n" + Constants.VERY_GOOD);
        }
        else if(counter >60){
            textMsg.setText(secondsToString(counter)+ "\n" +Constants.GOOD);
        }
        else{
            textMsg.setText(Constants.NULL);
        }

        text.setText(msg);

        dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(result){
                    activity.finish();
                }
                else{
                    //Do Nothing
                }
            }
        });

        dialog.show();

    }

    private String secondsToString(int pTime) {
        return String.format("%02d:%02d", pTime / 60, pTime % 60);
    }

}