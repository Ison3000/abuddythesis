package com.example.ison.abuddythesis.pojo;

import android.app.Application;

public class AppGlobalVariable extends Application {

    public boolean isMute() {
        return isMute;
    }

    public void setMute(boolean mute) {
        isMute = mute;
    }

    public boolean isLevel1ShapePassed() {
        return isLevel1ShapePassed;
    }

    public void setLevel1ShapePassed(boolean level1ShapePassed) {
        isLevel1ShapePassed = level1ShapePassed;
    }

    public boolean isLevel2ShapePassed() {
        return isLevel2ShapePassed;
    }

    public void setLevel2ShapePassed(boolean level2ShapePassed) {
        isLevel2ShapePassed = level2ShapePassed;
    }

    public boolean isLevel3ShapePassed() {
        return isLevel3ShapePassed;
    }

    public void setLevel3ShapePassed(boolean level3ShapePassed) {
        isLevel3ShapePassed = level3ShapePassed;
    }

    public boolean isLevel4ShapePassed() {
        return isLevel4ShapePassed;
    }

    public void setLevel4ShapePassed(boolean level4ShapePassed) {
        isLevel4ShapePassed = level4ShapePassed;
    }

    public boolean isLevel5ShapePassed() {
        return isLevel5ShapePassed;
    }

    public void setLevel5ShapePassed(boolean level5ShapePassed) {
        isLevel5ShapePassed = level5ShapePassed;
    }

    public boolean isLevel6ShapePassed() {
        return isLevel6ShapePassed;
    }

    public void setLevel6ShapePassed(boolean level6ShapePassed) {
        isLevel6ShapePassed = level6ShapePassed;
    }

    public boolean isLevel7ShapePassed() {
        return isLevel7ShapePassed;
    }

    public void setLevel7ShapePassed(boolean level7ShapePassed) {
        isLevel7ShapePassed = level7ShapePassed;
    }

    public boolean isLevel8ShapePassed() {
        return isLevel8ShapePassed;
    }

    public void setLevel8ShapePassed(boolean level8ShapePassed) {
        isLevel8ShapePassed = level8ShapePassed;
    }

    public boolean isLevel9ShapePassed() {
        return isLevel9ShapePassed;
    }

    public void setLevel9ShapePassed(boolean level9ShapePassed) {
        isLevel9ShapePassed = level9ShapePassed;
    }

    public boolean isLevel10ShapePassed() {
        return isLevel10ShapePassed;
    }

    public void setLevel10ShapePassed(boolean level10ShapePassed) {
        isLevel10ShapePassed = level10ShapePassed;
    }

    public boolean isLevel1PuzzlePassed() {
        return isLevel1PuzzlePassed;
    }

    public void setLevel1PuzzlePassed(boolean level1PuzzlePassed) {
        isLevel1PuzzlePassed = level1PuzzlePassed;
    }

    public boolean isLevel2PuzzlePassed() {
        return isLevel2PuzzlePassed;
    }

    public void setLevel2PuzzlePassed(boolean level2PuzzlePassed) {
        isLevel2PuzzlePassed = level2PuzzlePassed;
    }

    public boolean isLevel3PuzzlePassed() {
        return isLevel3PuzzlePassed;
    }

    public void setLevel3PuzzlePassed(boolean level3PuzzlePassed) {
        isLevel3PuzzlePassed = level3PuzzlePassed;
    }

    public boolean isLevel4PuzzlePassed() {
        return isLevel4PuzzlePassed;
    }

    public void setLevel4PuzzlePassed(boolean level4PuzzlePassed) {
        isLevel4PuzzlePassed = level4PuzzlePassed;
    }

    public boolean isLevel5PuzzlePassed() {
        return isLevel5PuzzlePassed;
    }

    public void setLevel5PuzzlePassed(boolean level5PuzzlePassed) {
        isLevel5PuzzlePassed = level5PuzzlePassed;
    }

    public boolean isLevel6PuzzlePassed() {
        return isLevel6PuzzlePassed;
    }

    public void setLevel6PuzzlePassed(boolean level6PuzzlePassed) {
        isLevel6PuzzlePassed = level6PuzzlePassed;
    }

    public boolean isLevel7PuzzlePassed() {
        return isLevel7PuzzlePassed;
    }

    public void setLevel7PuzzlePassed(boolean level7PuzzlePassed) {
        isLevel7PuzzlePassed = level7PuzzlePassed;
    }

    public boolean isLevel8PuzzlePassed() {
        return isLevel8PuzzlePassed;
    }

    public void setLevel8PuzzlePassed(boolean level8PuzzlePassed) {
        isLevel8PuzzlePassed = level8PuzzlePassed;
    }

    public boolean isLevel9PuzzlePassed() {
        return isLevel9PuzzlePassed;
    }

    public void setLevel9PuzzlePassed(boolean level9PuzzlePassed) {
        isLevel9PuzzlePassed = level9PuzzlePassed;
    }

    public boolean isLevel10PuzzlePassed() {
        return isLevel10PuzzlePassed;
    }

    public void setLevel10PuzzlePassed(boolean level10PuzzlePassed) {
        isLevel10PuzzlePassed = level10PuzzlePassed;
    }

    public boolean isLevel1EmotionPassed() {
        return isLevel1EmotionPassed;
    }

    public void setLevel1EmotionPassed(boolean level1EmotionPassed) {
        isLevel1EmotionPassed = level1EmotionPassed;
    }

    public boolean isLevel2EmotionPassed() {
        return isLevel2EmotionPassed;
    }

    public void setLevel2EmotionPassed(boolean level2EmotionPassed) {
        isLevel2EmotionPassed = level2EmotionPassed;
    }

    public boolean isLevel3EmotionPassed() {
        return isLevel3EmotionPassed;
    }

    public void setLevel3EmotionPassed(boolean level3EmotionPassed) {
        isLevel3EmotionPassed = level3EmotionPassed;
    }

    public boolean isLevel4EmotionPassed() {
        return isLevel4EmotionPassed;
    }

    public void setLevel4EmotionPassed(boolean level4EmotionPassed) {
        isLevel4EmotionPassed = level4EmotionPassed;
    }

    public boolean isLevel5EmotionPassed() {
        return isLevel5EmotionPassed;
    }

    public void setLevel5EmotionPassed(boolean level5EmotionPassed) {
        isLevel5EmotionPassed = level5EmotionPassed;
    }

    public boolean isLevel6EmotionPassed() {
        return isLevel6EmotionPassed;
    }

    public void setLevel6EmotionPassed(boolean level6EmotionPassed) {
        isLevel6EmotionPassed = level6EmotionPassed;
    }

    public boolean isLevel7EmotionPassed() {
        return isLevel7EmotionPassed;
    }

    public void setLevel7EmotionPassed(boolean level7EmotionPassed) {
        isLevel7EmotionPassed = level7EmotionPassed;
    }

    public boolean isLevel8EmotionPassed() {
        return isLevel8EmotionPassed;
    }

    public void setLevel8EmotionPassed(boolean level8EmotionPassed) {
        isLevel8EmotionPassed = level8EmotionPassed;
    }

    public boolean isLevel9EmotionPassed() {
        return isLevel9EmotionPassed;
    }

    public void setLevel9EmotionPassed(boolean level9EmotionPassed) {
        isLevel9EmotionPassed = level9EmotionPassed;
    }

    public boolean isLevel10EmotionPassed() {
        return isLevel10EmotionPassed;
    }

    public void setLevel10EmotionPassed(boolean level10EmotionPassed) {
        isLevel10EmotionPassed = level10EmotionPassed;
    }

    private boolean isMute;
    private boolean isLevel1ShapePassed;
    private boolean isLevel2ShapePassed;
    private boolean isLevel3ShapePassed;
    private boolean isLevel4ShapePassed;
    private boolean isLevel5ShapePassed;
    private boolean isLevel6ShapePassed;
    private boolean isLevel7ShapePassed;
    private boolean isLevel8ShapePassed;
    private boolean isLevel9ShapePassed;
    private boolean isLevel10ShapePassed;

    private boolean isLevel1PuzzlePassed;
    private boolean isLevel2PuzzlePassed;
    private boolean isLevel3PuzzlePassed;
    private boolean isLevel4PuzzlePassed;
    private boolean isLevel5PuzzlePassed;
    private boolean isLevel6PuzzlePassed;
    private boolean isLevel7PuzzlePassed;
    private boolean isLevel8PuzzlePassed;
    private boolean isLevel9PuzzlePassed;
    private boolean isLevel10PuzzlePassed;

    private boolean isLevel1EmotionPassed;
    private boolean isLevel2EmotionPassed;
    private boolean isLevel3EmotionPassed;
    private boolean isLevel4EmotionPassed;
    private boolean isLevel5EmotionPassed;
    private boolean isLevel6EmotionPassed;
    private boolean isLevel7EmotionPassed;
    private boolean isLevel8EmotionPassed;
    private boolean isLevel9EmotionPassed;
    private boolean isLevel10EmotionPassed;


}
